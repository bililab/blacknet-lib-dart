import 'dart:typed_data'; 
import 'package:hex/hex.dart'; // Hex
import 'package:bip_bech32/bip_bech32.dart'; // bech32

import 'package:blacknet_lib/blacknet.dart'; //blacknet

class Utils {
  
  static String toHex(Uint8List arr){
      return HEX.encode(arr).toUpperCase();
  }
  static Uint8List toByte(String str){
      List<int> isk = HEX.decode(str.toLowerCase());
      return Utils.int8list2uint8list(Int8List.fromList(isk));
  }
  static Uint8List int8list2uint8list(Int8List l) {
    Uint8List r = Uint8List(l.length);
    for (int i = 0; i < l.length; i++) {
      r[i] = l[i] & 0xFF;
    }
    return r;
  }
  static Uint8List publicKey(String account) {
      Bech32Codec bech32codec = Bech32Codec();
      return convertBits(bech32codec.decode(account).data, 5, 8, false);
  }
  static String publicKeyToAccount(Uint8List publicKey) {
      Uint8List cvtBytes = convertBits(publicKey, 8, 5, true);
      Bech32Codec bech32codec = Bech32Codec();
      return bech32codec.encode(Bech32(hrp, cvtBytes));
  }
  static String publicKeyToHex(Uint8List publicKey) {
      return HEX.encode(publicKey).toUpperCase();
  }
  static Uint8List hexToPublicKey(String str) {
      List<int> isk = HEX.decode(str.toLowerCase());
      return Utils.int8list2uint8list(Int8List.fromList(isk));
  }

  static Uint8List toUint8(int v) {
      Uint8List list = new Uint8List(1);
      list.buffer.asByteData().setUint8(0, v);
      return list;
  }

  static Uint8List toUint16(int v) {
      Uint8List list = new Uint8List(2);
      list.buffer.asByteData().setUint16(0, v);
      return list;
  }

  static Uint8List toUint32(int v) {
      Uint8List list = new Uint8List(4);
      list.buffer.asByteData().setUint32(0, v);
      return list;
  }

  static Uint8List toUint64(int v) {
      Uint8List list = new Uint8List(8);
      list.buffer.asByteData().setUint64(0, v);
      return list;
  }
}

// numberOfLeadingZeros .
int numberOfLeadingZeros(int i) {
    if (i == 0) {
        return 32;
    }
    var n = 1;
    if (i >> 16 == 0) {
        n += 16;
        i <<= 16;
    }
    if (i >> 24 == 0) {
        n += 8;
        i <<= 8;
    }
    if (i >> 28 == 0) {
        n += 4;
        i <<= 4;
    }
    if (i >> 30 == 0) {
        n += 2;
        i <<= 2;
    }
    n -= i >> 31;
    return n;
}
// encodeVarInt .
Uint8List encodeVarInt(int value) {
    int shift = 31 - numberOfLeadingZeros(value);
    // shift -= shift % 7;
    shift -= shift.remainder(7);
    List<int> arr = [];
    while (shift != 0) {
        arr.add(value >> shift & 0x7F);
        shift -= 7;
    }
    arr.add(value & 0x7F | 0x80);
    return Utils.int8list2uint8list(Int8List.fromList(arr));
}
// decodeVarInt
int decodeVarInt(Uint8List arr) {
    int ret = 0;
    int offset = 0;
    int v;
    do {
        ByteData bytes = arr.buffer.asByteData();
        v = bytes.getUint8(offset);
        ret = ret << 7 | (v & 0x7F);
        offset++;
    } while ((v & 0x80) == 0);
    return ret;
}