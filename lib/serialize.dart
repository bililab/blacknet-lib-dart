import 'package:dio/dio.dart';
import 'package:blacknet_lib/request.dart';
import 'package:blacknet_lib/blacknet/message.dart';
import 'package:blacknet_lib/blacknet/transaction.dart';
import 'package:blacknet_lib/blacknet/hash.dart';
import 'package:blacknet_lib/blacknet/utils.dart';
// Serialize
class Serialize {
  String endpoint;
  Request request;
  Serialize({this.endpoint}){
    this.request = new Request();
  }

  Future<Body> transfer(FormData form) async {
    Body body = await this.getSeq(form["from"]);
    if (body.code != 200) {
      return body;
    }
    Message m = Message(Message.plain, form["message"]);
    Transfer d = new Transfer(form["amount"], form["to"], m);
    Transaction t = new Transaction(form["from"], int.parse(body.body), Hash.empty(), form["fee"], 0, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> lease(FormData form) async {
    Body body = await this.getSeq(form["from"]);
    if (body.code != 200) {
      return body;
    }
    Lease d = new Lease(form["amount"], form["to"]);
    Transaction t = new Transaction(form["from"], int.parse(body.body), Hash.empty(), form["fee"], 2, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> cancelLease(FormData form) async {
    Body body = await this.getSeq(form["from"]);
    if (body.code != 200) {
      return body;
    }
    CancelLease d = new CancelLease(form["amount"], form["to"], form["height"]);
    Transaction t = new Transaction(form["from"], int.parse(body.body), Hash.empty(), form["fee"], 3, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> withdrawFromLease(FormData form) async {
    Body body = await this.getSeq(form["from"]);
    if (body.code != 200) {
      return body;
    }
    WithdrawFromLease d = new WithdrawFromLease(form["withdraw"], form["amount"], form["to"], form["height"]);
    Transaction t = new Transaction(form["from"], int.parse(body.body), Hash.empty(), form["fee"], 11, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> getSeq(String account) async {
    return this.request.get([
      this.endpoint+ "/api/v1/walletdb/getsequence",
      account
    ].join("/"));
  }
}