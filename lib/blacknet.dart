library blacknet_lib;

import 'dart:convert';
import 'dart:typed_data'; // Int8List
import 'package:blake2b/utils.dart' as butils; // butils
import 'package:bip39/bip39.dart' as bip39; // bip39
import 'package:bip_bech32/bip_bech32.dart'; // bech32
import 'package:blake2b/blake2b.dart'; // blake2b
import 'package:blake2b/blake2b_hash.dart'; // Blake2bHash
import 'package:blake2b/utils.dart'; // int8list2uint8list
import 'package:hex/hex.dart'; // Hex
import 'package:fixnum/fixnum.dart'; // int32
import 'package:ed25519/ed25519.dart'; // ed25519
import 'package:blacknet_lib/nacl.dart'; //nacl
import 'package:blacknet_lib/chacha20.dart'; //chacha20
import 'package:blacknet_lib/serialize.dart'; //serialize
import 'package:blacknet_lib/jsonrpc.dart'; //serialize

const sigma    = 'Blacknet Signed Message:\n';
const hrp      = 'blacknet';

/// A Blacknet.
class Blacknet {
  String endpoint;
  Serialize serialize;
  JSONRPC jsonrpc;
  Blacknet({this.endpoint}){
    this.serialize = new Serialize(endpoint: endpoint);
    this.jsonrpc = new JSONRPC(endpoint: endpoint);
  }
  /// hash
  static Uint8List hash(String mnemonic){
      // Uint8List msg = Uint8List.fromList(mnemonic.codeUnits);
      Uint8List msg = Uint8List.fromList(utf8.encode(mnemonic));
      return Blake2bHash.hash(msg, 0, msg.length);
  }
  /// 生成助记词
  static String generateMnemonic(){
      String mnemonic = "";
      while (true) {
        mnemonic = bip39.generateMnemonic();
        String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
        List<int> isk = HEX.decode(upHash);
        var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
        if((sk[0] & 0xF0) == 0x10){
          break;
        }
        mnemonic = "";
      }
      return mnemonic;
  }
  /// mnemonic 2 address
  static String address(String mnemonic){
    String hash = Blake2bHash.hashUtf8String2HexString(mnemonic);
    Uint8List hashBytes = Utils.int8list2uint8list(Int8List.fromList(HEX.decode(hash)));
    Uint8List pk = Ed25519.publickey(blake2bHashFunc, hashBytes);
    Uint8List cvtBytes = convertBits(pk, 8, 5, true);
    Bech32Codec bech32codec = Bech32Codec();
    return bech32codec.encode(Bech32(hrp, cvtBytes));
  }
  // sign message
  static String sign(String mnemonic, String message) {
  
    // Uint8List msg = Uint8List.fromList((sigma + message).codeUnits);
    Uint8List msg = Uint8List.fromList(utf8.encode(sigma + message));
    Uint8List hashMessage = Blake2bHash.hash(msg, 0, msg.length);

    var upperCaseHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upperCaseHash);
    var sk = butils.Utils.int8list2uint8list(Int8List.fromList(isk));
    var pk = Ed25519.publickey(blake2bHashFunc, sk);

    Uint8List sign = Ed25519.signature(blake2bHashFunc, hashMessage, sk, pk);
    String hexSign = HEX.encode(sign).toUpperCase();

    return hexSign;
  }
  /// verify signature
  static bool verify(String account, String signature, String message) {
    
    // Uint8List msg = Uint8List.fromList((sigma + message).codeUnits);
    Uint8List msg = Uint8List.fromList(utf8.encode(sigma + message));
    Uint8List hashMessage = Blake2bHash.hash(msg, 0, msg.length);

    Bech32Codec bech32codec = Bech32Codec();

    var publicKey = convertBits(bech32codec.decode(account).data, 5, 8, false);

    return Ed25519.checkvalid(blake2bHashFunc, HEX.decode(signature.toLowerCase()), hashMessage, publicKey);
  }
  // signature serialized
  static String signature(String mnemonic, String serialized){
    String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upHash);
    var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
    var pk = Ed25519.publickey(blake2bHashFunc, sk);
    List<int> imsg = HEX.decode(serialized);
    var signedMessage = Utils.int8list2uint8list(Int8List.fromList(imsg));
    const signatureSize = 64;
    Uint8List hash = Blake2bHash.hash(signedMessage, signatureSize, signedMessage.length - signatureSize);
    Uint8List signature = Ed25519.signature(blake2bHashFunc, hash, sk, pk);
    for(int i=0; i < signature.length; i++){
      signedMessage[i] = signature[i];
    }
    HEX.encode(signedMessage).toString();
    return HEX.encode(signedMessage).toString();
  }
  /// decrypt
  static String decrypt(String mnemonic, String account, String encrypt){
    String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upHash);
    var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
    Bech32Codec bech32codec = Bech32Codec();
    var pk = convertBits(bech32codec.decode(account).data, 5, 8, false);
    var publicKey = TweetNaclFast.convertPublicKey(pk);
    var secretKey = TweetNaclFast.convertSecretKey(sk);
    var key = TweetNaclFast.x25519(secretKey, publicKey);
    var encryptByte = Utils.int8list2uint8list(Int8List.fromList(HEX.decode(encrypt.toLowerCase())));
    var iv = encryptByte.sublist(0, 12);
    // return String.fromCharCodes(
    //   Chacha20.decryptUint8List(key, iv, encryptByte.sublist(12))
    // );
    return utf8.decode(Chacha20.decryptUint8List(key, iv, encryptByte.sublist(12)).toList());
  }
  /// encrypt
  static String encrypt(String mnemonic, String account, String encrypt){
    String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upHash);
    var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
    Bech32Codec bech32codec = Bech32Codec();
    var pk = convertBits(bech32codec.decode(account).data, 5, 8, false);
    var publicKey = TweetNaclFast.convertPublicKey(pk);
    var secretKey = TweetNaclFast.convertSecretKey(sk);
    var key = TweetNaclFast.x25519(secretKey, publicKey);
    var iv = TweetNaclFast.randombytes(12);
    // var encryptByte = Uint8List.fromList(encrypt.codeUnits);
    var encryptByte = Uint8List.fromList(utf8.encode(encrypt));
    var encrypted = Chacha20.encryptUint8List(key, iv, encryptByte);
    var out = new List<int>();
    out.addAll(iv);
    out.addAll(encrypted);
    return HEX.encode(out).toUpperCase();
  }
  static Uint8List publicKey(String account) {
      Bech32Codec bech32codec = Bech32Codec();
      return convertBits(bech32codec.decode(account).data, 5, 8, false);
  }
  static String publicKeyToAccount(Uint8List publicKey) {
      Uint8List cvtBytes = convertBits(publicKey, 8, 5, true);
      Bech32Codec bech32codec = Bech32Codec();
      return bech32codec.encode(Bech32(hrp, cvtBytes));
  }
}



Uint8List blake2bHashFunc(Uint8List m) {
  Uint8List bytes = Uint8List(64);
  var b = Blake2b(512);
  b.update(m, 0, m.length);
  b.digest(bytes, 0);
  return bytes;
}

Uint8List convertBits(Uint8List data, int from, int to, bool pad) {
  Int32 acc = Int32.parseInt("0");
  Int32 bits = Int32.parseInt("0");
  Int32 maxv = (Int32.ONE << to) - 1;
  List<int> ret = [];
  
  for (int i = 0; i < data.length; i++) {
    int b = data[i] & 0xFF;
    if (b < 0) {
      return null;
    } else if (b >> from > 0) {
      return null;
    }

    acc = acc << from | b;
    bits += from;
    while (bits >= to) {
      bits -= to;
      Int32 tmp = acc >> bits.toInt();
      tmp = tmp & maxv;
      ret.add(tmp.toInt());
    }
  }

  if (pad && bits > 0) {
    Int32 tmp = acc << to - bits.toInt() & maxv;
    ret.add(tmp.toInt());
  } else if(bits > from || (acc << to - bits.toInt() & maxv != 0)) {
    return null;
  }
  return Uint8List.fromList(ret);
}